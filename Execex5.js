const booksByCategory = [
    {
    category: "Riqueza",
    books: [
    {
    title: "Os segredos da mente milionária",
    author: "T. Harv Eker",
    },
    {
    title: "O homem mais rico da Babilônia",
    author: "George S. Clason",
    },
    {
    title: "Pai rico, pai pobre",
    author: "Robert T. Kiyosaki e Sharon L. Lechter",
    },
    ],
    },
    {
    category: "Inteligência Emocional",
    books: [
    {
    title: "Você é Insubstituível",
    author: "Augusto Cury",
    },
    {
    title: "Ansiedade – Como enfrentar o mal do século",
    author: "Augusto Cury",
    },
    {
    title: "Os 7 hábitos das pessoas altamente eficazes",
    author: "Stephen R. Covey"
    }
    ]
    }
    
    ];

//Para contar o número de autores:

function countAuthors(booksByCategory) {
    let authorsSet = new Set();

    for (let i = 0; i < booksByCategory.length; i++) {
        for (let j = 0; j < booksByCategory[i].books.length; j++) {
            authorsSet.add(booksByCategory[i].books[j].author);
        }
    }

    console.log(`Total de autores: ${authorsSet.size}`);
}

//countAuthors(booksByCategory);

//Para mostrar livros do autor Augusto Cury:

function showBooksByAugusto(booksByCategory) {
    let books = [];

    for (let i = 0; i < booksByCategory.length; i++) {
        for (let j = 0; j < booksByCategory[i].books.length; j++) {
            if (booksByCategory[i].books[j].author === "Augusto Cury") {
                books.push(booksByCategory[i].books[j]);
            }
        }
    }

    console.log(`Livros do autor Augusto Cury: `, books);
}





//Para transformar a função acima em uma função que irá receber o nome do autor e devolver os livros desse autor:

function showBooksByAuthor(booksByCategory, authorName) {
    let books = [];

    for (let i = 0; i < booksByCategory.length; i++) {
        for (let j = 0; j < booksByCategory[i].books.length; j++) {
            if (booksByCategory[i].books[j].author === authorName) {
                books.push(booksByCategory[i].books[j]);
            }
        }
    }

    console.log(`Livros do autor ${authorName}: `, books);
}




