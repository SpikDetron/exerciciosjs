function multiplyMatrices(a, b) {
    if (!Array.isArray(a) || !Array.isArray(b)) {
        throw new Error('As entradas devem ser matrizes');
    }

    let numRowsA = a.length;
    let numColsA = a[0].length;
    let numRowsB = b.length;
    let numColsB = b[0].length;

    if (numColsA !== numRowsB) {
        throw new Error('O número de colunas na primeira matriz deve ser igual ao número de linhas na segunda matriz');
    }

    let result = new Array(numRowsA);
    for (let r = 0; r < numRowsA; ++r) {
        result[r] = new Array(numColsB);
        for (let c = 0; c < numColsB; ++c) {
            result[r][c] = 0;
            for (let i = 0; i < numColsA; ++i) {
                result[r][c] += a[r][i] * b[i][c];
            }
        }
    }
    return result;
}

let testCase1 = [ [ [2],[-1] ], [ [2],[0] ] ];
let testCase2 = [ [2,3],[-2,1] ];
console.log(multiplyMatrices(testCase1, testCase2));

let testCase3 = [ [4,0], [-1,-1] ];
let testCase4 = [ [-1,3], [2,7] ];
console.log(multiplyMatrices(testCase3, testCase4));
