function fahrenheitParaCelsius(fahrenheit) {
    var celsius = (fahrenheit - 32) / 9 * 5;
    return celsius.toFixed(3);
}

var tempFahrenheit = 100;
var tempCelsius = fahrenheitParaCelsius(tempFahrenheit);
console.log(`A temperatura é ${tempCelsius} graus Celsius.`);
