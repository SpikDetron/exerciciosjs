function multiply(x, n) {
    if (n === 0) {
        return 0;
    } else {
        return x + multiply(x, n - 1);
    }
}

console.log(multiply(2, 3));
