let familyFinances = {
    receitas: [],
    despesas: []
};
function calculateBalance(familyFinances) {
    let totalReceitas = familyFinances.receitas.reduce((total, receita) => total + receita, 0);
    let totalDespesas = familyFinances.despesas.reduce((total, despesa) => total + despesa, 0);
    let saldo = totalReceitas - totalDespesas;

    if (saldo > 0) {
        console.log("A família está com saldo POSITIVO, totalizando: " + saldo);
    } else if (saldo < 0) {
        console.log("A família está com saldo NEGATIVO, totalizando: " + saldo);
    } else {
        console.log("A família está com saldo ZERO");
    }
}
